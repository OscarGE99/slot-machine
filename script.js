const leverBtn = document.getElementById("lever-ball");
const leverBarBtn = document.getElementById("lever-bar");
const leverBaseBBtn = document.getElementById("lever-base-big");
const leverBaseSBtn = document.getElementById("lever-base-small");
const timeRoll = 115;
const numFaces = 3;
var start = new Audio(
  "https://image.emails.sonymusicfans.com/lib/fe9212747567007476/m/1/2adc0d3d-eb2e-4ceb-a5cc-d71806d5270e.mp3"
);

const roll = (reel, offset, numRoll) => {
  const delta = (offset + 2) * numFaces + numRoll;
  const style = getComputedStyle(reel);
  const backgroundPositionY = parseFloat(style["background-position-y"]);
  reel.style.transition = `background-position-y ${
    8 + delta * timeRoll
  }ms cubic-bezier(.45,.05,.58,1.09)`;
  reel.style.backgroundPositionY = `${backgroundPositionY + delta * 240.6}px `;
};

function rollAll() {
  start.volume = 0.5;
  start.play();
  moveLever();

  const reelsList = document.querySelectorAll(".slots div");
  roll(reelsList[0], 1, 16);
  roll(reelsList[1], 5, 8);
  roll(reelsList[2], 6, 9);
  leverBtn.removeEventListener("click", rollAll);
  leverBarBtn.removeEventListener("click", rollAll);
  leverBaseBBtn.removeEventListener("click", rollAll);
  leverBaseSBtn.removeEventListener("click", rollAll);
  document.querySelector("#lever-ball").style.cursor = "auto";
  document.querySelector("#lever-ball").classList.remove("clic");
  document.querySelector("#lever-bar").classList.remove("clic2");
  document.querySelector("#lever-base-big").classList.remove("clic3");
  document.querySelector("#lever-base-small").classList.remove("clic3");
  setTimeout(() => {
    document.querySelector(".slots").classList.add("win");
    document.querySelector(".machine-title").classList.add("jackpot-win");
  }, 4000);
  setTimeout(() => {
    document.querySelector(".slots").classList.remove("win");
    document.querySelector(".machine-title").classList.remove("jackpot-win");
    document.querySelector(".machine-title").style.color = "#d3342e";
    document.querySelector(".machine-title").style.textShadow =
      "0 0 10px rgba(255, 255, 255, 0.2)";
  }, 6000);
  setTimeout(() => {
    reelsList[0].style.display = "none";
    reelsList[1].style.display = "none";
    reelsList[2].style.display = "none";
    document.querySelector(".original-image").style.display = "flex";
    document.querySelector(".slots").style.justifyContent = "center";
  }, 6000);
}

function moveLever() {
  $("#lever-ball").animate(
    { height: "7em", width: "7em", bottom: "2.5em", right: "4em" },
    300,
    function () {
      $("#lever-ball").animate(
        { height: "7em", width: "7em", bottom: "11.5em", right: "1em" },
        800
      );
    }
  );
  $("#lever-bar").animate({ height: "22em", bottom: "11em" }, 300, function () {
    $("#lever-bar").animate({ height: "30em", bottom: "13em" }, 800);
  });
}

leverBtn.addEventListener("click", rollAll);
leverBarBtn.addEventListener("click", rollAll);
leverBaseBBtn.addEventListener("click", rollAll);
leverBaseSBtn.addEventListener("click", rollAll);
